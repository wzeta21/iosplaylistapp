//
//  Database.swift
//  iOsPlaylistApp
//
//  Created by zeta on 10/23/18.
//  Copyright © 2018 avantica training. All rights reserved.
//

import Foundation
import RealmSwift

class Database {
    
    static func saveSong(_ song: SongR){
        let realm = try! Realm()
        if realm.object(ofType: SongR.self, forPrimaryKey: song.trackId) != nil {
            return
        }
        try! realm.write {
            realm.add(song)
        }
    }
    
    static func deleteSong(_ song: SongR) {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(song)
        }
    }
   
    static func deleteSongByName(_ trackName: String) {
        let realm = try! Realm()
        
        let predicate = NSPredicate(format: "trackName == %@", "\(trackName)")
        let filterSongs = realm.objects(SongR.self).filter(predicate)
        
        if let toDelete = filterSongs.first {
            try! realm.write {
                realm.delete(toDelete)
            }
        }
    }
    
    static func fetchSongs() -> [SongR] {
        let realm = try! Realm()
        let songs = realm.objects(SongR.self)
        return Array(songs)
    }
    
    static func exist(_ song: SongR) -> Bool {
        let realm = try! Realm()
        if realm.object(ofType: SongR.self, forPrimaryKey: song.trackId) != nil {
            return true
        }else {
            return false
        }
    }
    
}
