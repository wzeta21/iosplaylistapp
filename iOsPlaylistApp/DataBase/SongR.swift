//
//  SongR.swift
//  iOsPlaylistApp
//
//  Created by zeta on 10/23/18.
//  Copyright © 2018 avantica training. All rights reserved.
//

import Foundation
import RealmSwift
class SongR: Object  {
    @objc dynamic var trackId: Int = 0
    @objc dynamic var trackName: String = ""
    @objc dynamic var artistId: String = ""
    @objc dynamic var artistName: String = ""
    @objc dynamic var artistUrl100: String = ""
    @objc dynamic var collectionName: String = ""
    @objc dynamic var releaseDate: String = ""
    @objc dynamic var trackViewUrl: String = ""
    @objc dynamic var artworkUrl100: String = ""
    
    override static func primaryKey() -> String? {
        return "trackId"
    }
}
