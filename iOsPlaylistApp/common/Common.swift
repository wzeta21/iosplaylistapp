//
//  Common.swift
//  iOsPlaylistApp
//
//  Created by zeta on 11/16/18.
//  Copyright © 2018 avantica training. All rights reserved.
//

import Foundation
import SDWebImage

class Common {
    static let Url: String = "https://itunes.apple.com/search?term=*&entity=song"
    static func releaseDateFormater(releaseDate: String) -> String{
        var dateString:String = ""
        let df = DateFormatter()
        df.locale = Locale(identifier: "en_US_POSIX")
        df.dateFormat = "yyyy-MM-dd'T'HH:mm:ssX"
        if let date = df.date(from: releaseDate){
            let df2 = DateFormatter()
            df2.dateStyle = .medium
            dateString = df2.string(from: date)
        }
        return dateString
    }
    
    static func trackImageBinding(_ trackImg: inout UIImageView!, url: String) {
        let imgUrl:NSURL? = NSURL(string: url)
        
        trackImg.sd_setImage(with: imgUrl! as URL, completed: { (image: UIImage?, error: NSError?, cacheType: SDImageCacheType, imageUrl: URL?) in
            print("Error loading image: \(String(describing: error))")
            } as? SDExternalCompletionBlock)
    }
}
