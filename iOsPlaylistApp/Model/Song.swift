//
//  Song.swift
//  iOsPlaylistApp
//
//  Created by zeta on 10/23/18.
//  Copyright © 2018 avantica training. All rights reserved.
//

import UIKit

class Song: NSObject {
    let trackId: Int
    let trackName: String
    let artistId: String
    let artistName: String
    let artworkUrl100: String
    let collectionName: String
    let releaseDate: String
    let trackViewUrl: String
    
    init?(dictionary: [String: Any]) {
        self.trackId = dictionary["trackId"] as? Int ?? 0
        self.trackName = dictionary["trackName"] as? String ?? ""
        self.artistId = dictionary["artistId"] as? String ?? ""
        self.artistName = dictionary["artistName"] as? String ?? ""
        self.artworkUrl100 = dictionary["artworkUrl100"] as? String ?? ""
        self.collectionName = dictionary["collectionName"] as? String ?? ""
        self.releaseDate = dictionary["releaseDate"] as? String ?? ""
        self.trackViewUrl = dictionary["trackViewUrl"] as? String ?? ""
    }

}
