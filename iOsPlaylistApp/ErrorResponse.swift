//
//  ErrorResponse.swift
//  iOsPlaylistApp
//
//  Created by zeta on 10/23/18.
//  Copyright © 2018 avantica training. All rights reserved.
//

import Foundation

struct ErrorResponse {
    let message: String
    init(message: String)
    {
        self.message = message
    }
}
