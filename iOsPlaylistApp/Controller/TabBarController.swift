//
//  TabBarController.swift
//  iOsPlaylistApp
//
//  Created by zeta on 11/16/18.
//  Copyright © 2018 avantica training. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
