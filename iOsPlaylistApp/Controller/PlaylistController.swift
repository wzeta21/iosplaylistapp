//
//  ViewController.swift
//  iOsPlaylistApp
//
//  Created by zeta on 10/23/18.
//  Copyright © 2018 avantica training. All rights reserved.
//

import UIKit

class PlaylistController: UITableViewController {

    var track: Song?
    var tracks: [Song] = [Song]()
    var searchSong: [Song] = [Song]()
    @IBOutlet weak var searchBar: UISearchBar!
    var searching: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        getTracks()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searching ? searchSong.count : tracks.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "playlistCell", for: indexPath) as! PlaylistCell
        if searching {
            cell.lblTrackName.text = searchSong[indexPath.row].trackName
            cell.lblAlbum.text = searchSong[indexPath.row].artistName
        }  else {
            cell.lblTrackName.text = tracks[indexPath.row].trackName
            cell.lblAlbum.text = tracks[indexPath.row].artistName
        }
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? DetailTrackViewController{
            destinationVC.track = self.track
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = tracks[indexPath.row]
        track = data
        self.performSegue(withIdentifier: "showDetailVC", sender: self)
        //print("track data: \(data)")
    }
    /*
    func getData(){
        
        ServiceManager.getInstance().getSong(success: {(song) in
            DispatchQueue.main.async {
                print("song: \(song.trackName)")
                print("song id: \(song.trackId)")
                print("artist name: \(song.artistName)")
                print("successful")
            }
        }, failure: {(error) in
            print("\(error)")
        })
    }*/
    
    func getTracks(){
       ServiceManager.getInstance().getSongs(success: {(songs) in
            DispatchQueue.main.async {
               self.tracks = songs
                print("successful")
                self.tableView.reloadData()
            }
        }, failure: {(error) in
            print("\(error)")
       })
    }
}

extension PlaylistController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        searchSong = tracks.filter({$0.trackName.prefix(searchText.count) == searchText  || $0.artistName.prefix(searchText.count) == searchText})
        searching = true
        self.tableView.reloadData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searching = false
        searchBar.text = ""
        self.tableView.reloadData()
    }
}
