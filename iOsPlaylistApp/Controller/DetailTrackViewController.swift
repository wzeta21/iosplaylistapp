//
//  DetailTrackViewController.swift
//  iOsPlaylistApp
//
//  Created by zeta on 11/6/18.
//  Copyright © 2018 avantica training. All rights reserved.
//

import UIKit
import SDWebImage
class DetailTrackViewController: UIViewController {

    @IBOutlet weak var lblColectionName: UILabel!
    @IBOutlet weak var trackImg: UIImageView!
    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var artistName: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    var track: Song?
    override func viewDidLoad() {
        super.viewDidLoad()

        trackName.text = track?.trackName
        artistName.text = track?.artistName
        Common.trackImageBinding(&trackImg, url: (track?.artworkUrl100)!)
        lblReleaseDate.text = Common.releaseDateFormater(releaseDate: (track?.releaseDate)!)
        lblColectionName.text = track?.collectionName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func addToFavorites(_ sender: UIButton) {
        createAler(title: "Adding to favorite list", message: "Do you want to add?")
    }
    
    func createAler(title:String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "No", style: UIAlertActionStyle.default, handler:{(action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler:{(action) in
            alert.dismiss(animated: true, completion: nil)
            self.saveTrack(song: self.track!)
        }))        
        self.present(alert, animated: true, completion: nil)
    }
    
    func saveTrack(song: Song){
        let trackR = SongR()
        trackR.artistId = song.artistId
        trackR.artistName = song.artistName
        trackR.artistUrl100 = song.artworkUrl100
        trackR.collectionName = song.collectionName
        trackR.releaseDate = song.releaseDate
        trackR.trackId = song.trackId
        trackR.trackName = song.trackName
        trackR.trackViewUrl = song.trackViewUrl
        trackR.artworkUrl100 = song.artworkUrl100
        
        Database.saveSong(trackR)
    }
}

