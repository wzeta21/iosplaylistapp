//
//  DetalleController.swift
//  iOsPlaylistApp
//
//  Created by zeta on 11/13/18.
//  Copyright © 2018 avantica training. All rights reserved.
//

import UIKit
import SDWebImage
class FavoriteController: UIViewController {

    var tracks: [Song] = [Song]()

    @IBOutlet weak var favoriteTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.rightBarButtonItem = editButtonItem
    }
    override func viewWillAppear(_ animated: Bool) {
        getTracksFromDb()
        favoriteTable.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        tracks = [Song]()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getTracksFromDb(){
        let songs = Database.fetchSongs()
        for s in songs {
            let internalTrack = Song(dictionary: [
                "trackName":s.trackName,
                "artistName":s.artistName,
                "releaseDate":s.releaseDate,
                "collectionName": s.collectionName,
                "artworkUrl100": s.artistUrl100,
                "trackViewUrl": s.trackViewUrl
                ])
            tracks.append(internalTrack!)
        }
    }
    
    
}
extension FavoriteController: FavoriteCellDelegate{
    func didShareTrack(track: Song) {
        let activityVC = UIActivityViewController(activityItems: [track.trackViewUrl], applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
        print("sharing: \(String(describing: track.trackViewUrl))")
    }
}
extension FavoriteController:  UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tracks.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favoritesCell", for: indexPath) as! FavoriteCell
        cell.lblTrackName.text = tracks[indexPath.row].trackName
        cell.lblArtistName.text = tracks[indexPath.row].artistName        
        cell.lblReleaseDate.text = Common.releaseDateFormater(releaseDate: tracks[indexPath.row].releaseDate)
        cell.lblCollectionName.text = tracks[indexPath.row].collectionName
        cell.track = tracks[indexPath.row]
        Common.trackImageBinding(&cell.imgTrackPicture, url: (tracks[indexPath.row].artworkUrl100))
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 211
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let trackToDelte = tracks[indexPath.row]
            tracks.remove(at: indexPath.row)
            Database.deleteSongByName(trackToDelte.trackName)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        }
    }
}
