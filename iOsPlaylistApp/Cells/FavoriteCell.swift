//
//  FavoriteTableViewCell.swift
//  iOsPlaylistApp
//
//  Created by zeta on 11/13/18.
//  Copyright © 2018 avantica training. All rights reserved.
//

import UIKit

protocol FavoriteCellDelegate {
    func didShareTrack(track: Song)
}

class FavoriteCell: UITableViewCell {

    @IBOutlet weak var lblTrackName: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    
    @IBOutlet weak var lblCollectionName: UILabel!
    @IBOutlet weak var imgTrackPicture: UIImageView!
    @IBOutlet weak var lblArtistName: UILabel!
    var track: Song?
    var delegate: FavoriteCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBAction func btnShare(_ sender: UIButton) {
        delegate?.didShareTrack(track: track!)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
