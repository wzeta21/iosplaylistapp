//
//  PlaylistCell.swift
//  iOsPlaylistApp
//
//  Created by zeta on 10/23/18.
//  Copyright © 2018 avantica training. All rights reserved.
//

import UIKit

class PlaylistCell: UITableViewCell {

    @IBOutlet weak var lblAlbum: UILabel!
    @IBOutlet weak var lblTrackName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
