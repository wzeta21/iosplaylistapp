//
//  Service.swift
//  iOsPlaylistApp
//
//  Created by zeta on 10/23/18.
//  Copyright © 2018 avantica training. All rights reserved.
//

import Foundation
import Alamofire
class ServiceManager {
    
    private static var instance = ServiceManager()
    
    private init(){
    }
    
    static func getInstance() -> ServiceManager{
        if let _: ServiceManager = self.instance {
            return self.instance
            
        } else {
            self.instance = ServiceManager()
        }
        return self.instance
    }
    func getSong(success: @escaping(_ song: Song) -> (), failure: @escaping(_ errorResponse: ErrorResponse) ->()){
        let url :String = Common.Url //"https://itunes.apple.com/search?term=*&entity=song"
        Alamofire.request(url).responseJSON { response in
            
            guard response.result.error == nil else {
                print("Error: \(String(describing: response.error))")
                let e = ErrorResponse(message: response.result.error as! String)
                failure(e)
                return
            }
            
            guard let json = response.result.value as? [String: Any] else {
                print("didn't get todo object as JSON from API")
                if response.result.error != nil {
                    let e = ErrorResponse(message: response.error as! String)
                    failure(e)
                }
                return
            }
            
            guard let tracks = json["results"] as? [[String: Any]] else {
                let e = ErrorResponse(message: "Error try get tracks")
                failure(e)
                return
            }
        
            let track = tracks.first
            
            //print("Track: \(track!)")
            let sangs = Song(dictionary: track!)
            //print("Track: \(sangs!)")
            success(sangs!)
        }
    }
    
    func getSongs(success: @escaping(_ songs: [Song]) -> (), failure: @escaping(_ errorResponse: ErrorResponse) ->()) {
        let url : String = "https://itunes.apple.com/search?term=*&entity=song"
        Alamofire.request(url).responseJSON { response in
            
            guard response.result.error == nil else {
                print("Error: \(String(describing: response.error))")
                let e = ErrorResponse(message: response.result.error as! String)
                failure(e)
                return
            }
            
            guard let json = response.result.value as? [String: Any] else {
                print("didn't get todo object as JSON from API")
                if response.result.error != nil {
                    let e = ErrorResponse(message: response.error as! String)
                    failure(e)
                }
                return
            }
            
            guard let tracks = json["results"] as? [[String: Any]] else {
                let e = ErrorResponse(message: "Error try get tracks")
                failure(e)
                return
            }
            
            var songs:[Song] = []
            
            for s in tracks {
                //print("songs:\(s)")
                songs.append(Song(dictionary: s)!)
            }
            
            //print("Track counting: \(songs.count)")
            //let sangs = Song(dictionary: track!)
            success(songs)
        }
    }
}
